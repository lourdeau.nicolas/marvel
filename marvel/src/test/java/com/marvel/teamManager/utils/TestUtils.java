package com.marvel.teamManager.utils;

import java.util.ArrayList;
import java.util.List;

import com.marvel.teamManager.model.BestTeam;
import com.marvel.teamManager.model.Hero;
import com.marvel.teamManager.model.User;
import com.marvel.teamManager.model.UserCredentials;

/**
 * Regroupe des fonctions de mock utillisées par les classes de test
 * 
 * @author Caradoc
 *
 */
public class TestUtils {

	private static final String MDP = "JAMBONBEURRE";
	private static final String LOGIN = "EDOUARD";

	protected User mockUser() {
		User user = new User(LOGIN, MDP);
		user.setBestTeam(mockBestTeam());
		return user;
	}

	protected BestTeam mockBestTeam() {
		BestTeam bestTeam = new BestTeam();
		Hero hero = new Hero("Hulk", 42);
		Hero hero2 = new Hero("Hulkette", 56);
		List<Hero> heroes = new ArrayList<>();
		heroes.add(hero);
		heroes.add(hero2);
		bestTeam.setHeroes(heroes);
		return bestTeam;
	}

	protected UserCredentials mockUserCredentials() {
		return new UserCredentials(LOGIN, MDP);
	}

}
