package com.marvel.teamManager.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.marvel.teamManager.model.User;
import com.marvel.teamManager.model.UserToken;
import com.marvel.teamManager.repository.UserRepo;
import com.marvel.teamManager.repository.UserTokenRepo;
import com.marvel.teamManager.utils.TestUtils;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
class UserServiceTest extends TestUtils {

	@InjectMocks
	private UserService userService;

	@Mock
	private UserTokenRepo userTokenRepoMock;

	@Mock
	private UserRepo userRepoMock;

	@Captor
	ArgumentCaptor<UserToken> tokenCaptor;

	@Test
	public void connectUserNominal() throws Exception {
		User user = mockUser();

		when(userRepoMock.findByLoginAndMdp(user.getLogin(), user.getMdp())).thenReturn(user);

		String response = userService.connectUser(user);

		Mockito.verify(userTokenRepoMock).save(Mockito.any(UserToken.class));
		Mockito.verify(userTokenRepoMock).save(tokenCaptor.capture());
		UserToken capturatedToken = tokenCaptor.getValue();

		assertNotNull(response);
		assertEquals(capturatedToken.getToken(), response);

	}

}
