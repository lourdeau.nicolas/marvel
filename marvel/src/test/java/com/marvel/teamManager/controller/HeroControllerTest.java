package com.marvel.teamManager.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.marvel.teamManager.model.Hero;
import com.marvel.teamManager.repository.HeroRepo;
import com.marvel.teamManager.service.HeroService;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
class HeroControllerTest {

	@InjectMocks
	private HeroController heroController;

	@Mock
	private HeroService heroServiceMock;

	@Mock
	private HeroRepo heroRepoMock;

	@Test
	public void isFavoriteNominal() throws Exception {
		int idMock = 42;
		String tokenMock = "tokenMock";

		when(heroServiceMock.isFavorite(idMock, tokenMock)).thenReturn(true);
		boolean response = heroController.isFavorite(idMock, tokenMock);
		assertTrue(response);

		when(heroServiceMock.isFavorite(idMock, tokenMock)).thenReturn(false);
		response = heroController.isFavorite(idMock, tokenMock);
		assertFalse(response);
	}

	@Test
	public void getHeroesNominal() throws Exception {
		Hero hero = new Hero("Hulk", 42);
		List<Hero> heroes = new ArrayList<>();
		heroes.add(hero);

		when(heroRepoMock.findAll()).thenReturn(heroes);
		Iterable<Hero> response = heroController.getAllHeroes();
		assertNotNull(response);
		assertEquals(response.toString(), heroes.toString());

	}
}
