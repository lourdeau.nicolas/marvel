/**
 * 
 */
package com.marvel.teamManager.service;

import java.nio.file.AccessDeniedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marvel.teamManager.model.Hero;
import com.marvel.teamManager.model.User;
import com.marvel.teamManager.repository.HeroRepo;
import com.marvel.teamManager.repository.UserRepo;

/**
 * Contient les méthodes agissant sur les meilleures équipes
 * 
 * @author Caradoc
 *
 */
@Service
public class BestTeamService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private HeroRepo heroRepo;

	@Autowired
	private UserService userService;

	public void addHeroToTeam(String name, int id, User user) {
		Hero hero = heroRepo.findById(id);
		if (hero == null) {
			hero = new Hero(name, id);
			heroRepo.save(hero);
		}
		user.getBestTeam().getHeroes().add(hero);
		userRepo.save(user);
	}

	public Iterable<Hero> getTeam(User user) {
		return user.getBestTeam().getHeroes();
	}

	public String removeHeroFromTeam(int id, String token) throws AccessDeniedException {

		User user = userService.getUserFromToken(token);
		user.getBestTeam().getHeroes().removeIf(hero -> hero.getId() == id);
		userRepo.save(user);
		return "Hero Removed";
	}

}
