/**
 * 
 */
package com.marvel.teamManager.service;

import java.nio.file.AccessDeniedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marvel.teamManager.model.User;

/**
 * fcontion permettant d'agir sur un héro
 * 
 * @author Caradoc
 *
 */
@Service
public class HeroService {

	@Autowired
	private UserService userService;

	public boolean isFavorite(int id, String token) throws AccessDeniedException {
		User user = userService.getUserFromToken(token);

		return user.getBestTeam().getHeroes().stream().anyMatch(hero -> hero.getId() == id);
	}

}
