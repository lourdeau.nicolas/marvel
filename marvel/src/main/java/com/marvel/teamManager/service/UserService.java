/**
 * 
 */
package com.marvel.teamManager.service;

import java.nio.file.AccessDeniedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marvel.teamManager.model.User;
import com.marvel.teamManager.model.UserCredentials;
import com.marvel.teamManager.model.UserToken;
import com.marvel.teamManager.repository.UserRepo;
import com.marvel.teamManager.repository.UserTokenRepo;

/**
 * @author Caradoc
 *
 */
@Service
public class UserService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private UserTokenRepo userTokenRepo;

	/**
	 * Vérifie que l'utilisateur existe et crée un Token d'authentification
	 * 
	 * @param userCredentials
	 * @return un jeton d'identification
	 * @throws AccessDeniedException
	 */
	public String connectUser(UserCredentials userCredentials) throws AccessDeniedException {
		User user = userRepo.findByLoginAndMdp(userCredentials.getLogin(), userCredentials.getMdp());
		// Vérification que l'utilisateur existe
		if (user == null) {
			throw new AccessDeniedException("Identifiant ou mot de passe incorrect");
		}

		UserToken userToken = new UserToken(user);
		userTokenRepo.save(userToken);
		return userToken.getToken();

	}

	public User getUserFromToken(String token) throws AccessDeniedException {
		User user = userTokenRepo.getByToken(token).getUser();
		if (user == null) {
			throw new AccessDeniedException("Session Expirée");
		}
		return user;
	}

}
