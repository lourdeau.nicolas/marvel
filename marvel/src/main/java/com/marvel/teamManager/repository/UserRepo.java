package com.marvel.teamManager.repository;

import org.springframework.stereotype.Repository;

import com.marvel.teamManager.model.User;

@Repository
public interface UserRepo extends UserCredentialsRepo<User> {

}
