package com.marvel.teamManager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.marvel.teamManager.model.UserCredentials;

@NoRepositoryBean
public interface UserCredentialsRepo<T extends UserCredentials> extends JpaRepository<T, String> {

	T findByLogin(String nom);

	T findByLoginAndMdp(String nom, String mdp);

}
