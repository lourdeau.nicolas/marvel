package com.marvel.teamManager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marvel.teamManager.model.UserToken;

@Repository
public interface UserTokenRepo extends JpaRepository<UserToken, String> {

	UserToken getByToken(String token);

}
