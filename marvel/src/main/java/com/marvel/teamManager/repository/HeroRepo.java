package com.marvel.teamManager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marvel.teamManager.model.Hero;

@Repository
public interface HeroRepo extends JpaRepository<Hero, String> {

	Hero findByName(String nom);

	Hero findById(int id);

}
