/**
 * 
 */
package com.marvel.teamManager.model;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author Caradoc
 *
 */
@Entity
public class UserToken {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int tokenId;

	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	private String token = generateNewToken();

	private Date dateToken = new Date();

	private static final SecureRandom secureRandom = new SecureRandom();
	private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder();

	public static String generateNewToken() {
		byte[] randomBytes = new byte[24];
		secureRandom.nextBytes(randomBytes);
		return base64Encoder.encodeToString(randomBytes);
	}

	public UserToken(User user) {
		super();
		this.user = user;
	}

	/**
	 * Constructeur par défaut
	 */
	public UserToken() {
		super();
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the dateToken
	 */
	public Date getDateToken() {
		return dateToken;
	}

	/**
	 * @param dateToken the dateToken to set
	 */
	public void setDateToken(Date dateToken) {
		this.dateToken = dateToken;
	}
}
