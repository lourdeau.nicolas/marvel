package com.marvel.teamManager.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class User extends UserCredentials {

	/**
	 * Id de l'utilisateur dans la BDD
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sélection de Héros préférés de l'utilisateur
	 */
	@OneToOne(cascade = CascadeType.PERSIST)
	private BestTeam bestTeam;

	/**
	 * Constructeur
	 * 
	 * @param login Identifiant de l'utilisateur
	 * @param mdp   Mot de passe de l'utilisateur
	 */
	public User(String login, String mdp) {
		super(login, mdp);
		this.bestTeam = new BestTeam();
	}

	/**
	 * Constructeur
	 * 
	 * @param login Identifiant de l'utilisateur
	 * @param mdp   Mot de passe de l'utilisateur
	 */
	public User(UserCredentials userCredentials) {
		super(userCredentials);
		this.bestTeam = new BestTeam();
	}

	/**
	 * Constructeur par défaut pour JPA
	 * 
	 */
	public User() {
		super(null, null);
	}

	/**
	 * @return the bestTeam
	 */
	public BestTeam getBestTeam() {
		return bestTeam;
	}

	/**
	 * @param bestTeam the bestTeam to set
	 */
	public void setBestTeam(BestTeam bestTeam) {
		this.bestTeam = bestTeam;
	}

}
