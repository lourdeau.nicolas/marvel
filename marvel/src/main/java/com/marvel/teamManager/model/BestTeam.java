/**
 * 
 */
package com.marvel.teamManager.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Représente l'ensemble de héros favoris d'un utilisateur
 * @author Caradoc
 *
 */
@Entity
public class BestTeam {

	/**
	 * Identifiant de l'équipe
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idTeam;
	
	/**
	 * Collection de héros
	 */
	@OneToMany
	private Collection<Hero> heroes = new ArrayList<Hero>();
	
	/**
	 * retourne l'identifiant de l'équipe
	 * @return l'identifiant de l'équipe
	 */
	public int getIdTeam() {
		return idTeam;
	}

	/**
	 * affecte l'identifiant de l'équipe
	 * @param id l'identifiant de l'équipe
	 */
	public void setIdTeam(int id) {
		this.idTeam = id;
	}

	/**
	 * obtient la liste des héros
	 * @return la liste des héros favoris
	 */
	public Collection<Hero> getHeroes() {
		return heroes;
	}

	/**
	 * 
	 * @param heroes the heroes to set
	 */
	public void setHeroes(List<Hero> heroes) {
		this.heroes = heroes;
	}

}
