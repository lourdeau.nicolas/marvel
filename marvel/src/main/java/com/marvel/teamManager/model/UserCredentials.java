/**
 * 
 */
package com.marvel.teamManager.model;

import javax.persistence.MappedSuperclass;

/**
 * @author Caradoc
 *
 */
@MappedSuperclass
public class UserCredentials {
	/**
	 * Identifiant de l'utilisateur
	 */
	private String login;
	/**
	 * Mot de passe de l'utilisateur
	 */
	private String mdp;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	/**
	 * @param login
	 * @param mdp
	 */
	public UserCredentials(String login, String mdp) {
		super();
		this.login = login;
		this.mdp = mdp;
	}

	/**
	 * @param login
	 * @param mdp
	 */
	public UserCredentials(UserCredentials userCredentials) {
		super();
		this.login = userCredentials.getLogin();
		this.mdp = userCredentials.getMdp();
	}

}
