package com.marvel.teamManager.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Hero {

	@Id
	private int id;

	private String name;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param nom
	 */
	public Hero(String name, int id) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * Constructeur par défaut
	 * 
	 * @param name
	 */
	public Hero() {
		super();
	}

}
