package com.marvel.teamManager.controller;

import java.nio.file.AccessDeniedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.marvel.teamManager.model.Hero;
import com.marvel.teamManager.repository.HeroRepo;
import com.marvel.teamManager.service.HeroService;

@CrossOrigin
@RestController
@RequestMapping(path = "/hero")
public class HeroController {

	@Autowired
	private HeroRepo heroRepo;

	@Autowired
	private HeroService heroService;

	@PostMapping(path = "/add")
	public @ResponseBody String addHero(@RequestParam String nom, @RequestParam int id) {
		Hero hero = new Hero(nom, id);
		heroRepo.save(hero);
		return "hero saved";
	}

	@GetMapping(path = "/all")
	public @ResponseBody Iterable<Hero> getAllHeroes() {
		return heroRepo.findAll();
	}

	@GetMapping(path = "/fav")
	public @ResponseBody boolean isFavorite(@RequestParam int id, @RequestParam String token)
			throws AccessDeniedException {
		return heroService.isFavorite(id, token);
	}
}
