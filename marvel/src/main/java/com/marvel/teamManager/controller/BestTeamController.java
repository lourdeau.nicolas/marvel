package com.marvel.teamManager.controller;

import java.nio.file.AccessDeniedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.marvel.teamManager.model.Hero;
import com.marvel.teamManager.model.User;
import com.marvel.teamManager.service.BestTeamService;
import com.marvel.teamManager.service.UserService;

/**
 * Exposition des services appliquables à la meilleure équipe
 * 
 * @author Caradoc
 *
 */
@CrossOrigin
@RestController
@RequestMapping(path = "/team")
public class BestTeamController {

	@Autowired
	private BestTeamService bestTeamService;

	@Autowired
	private UserService userService;

	/**
	 * Ajoute un héro à l'équipe
	 * 
	 * @param name  nom du héro
	 * @param id    identifiant du héro
	 * @param token jeton de session de l'utilisateur
	 * @return réponse OK + phrase correspondant à l'action effectuée
	 * @throws AccessDeniedException utilisateur incorrect
	 */
	@PostMapping(path = "/add")
	public @ResponseBody String addHeroToBestTeam(@RequestParam String name, @RequestParam int id,
			@RequestParam String token) throws AccessDeniedException {
		User user = userService.getUserFromToken(token);
		bestTeamService.addHeroToTeam(name, id, user);
		return "hero saved";
	}

	/**
	 * Retire un héro de l'équipe
	 * 
	 * @param id    identifiant du héro
	 * @param token jeton de session de l'utilisateur
	 * @return réponse OK + phrase correspondant à l'action effectuée
	 * @throws AccessDeniedException
	 */
	@PostMapping(path = "/del")
	public @ResponseBody String removeHeroFromBestTeam(@RequestParam int id, @RequestParam String token)
			throws AccessDeniedException {
		return bestTeamService.removeHeroFromTeam(id, token);
	}

	/**
	 * Retourne la liste de héro faisant partie de l'équipe favorite de
	 * l'utilisateur
	 * 
	 * @param token jeton de session de l'utilisateur
	 * @return la liste de héros
	 * @throws AccessDeniedException
	 */
	@GetMapping(path = "/all")
	public @ResponseBody Iterable<Hero> getAllHeroes(@RequestParam String token) throws AccessDeniedException {
		User user = userService.getUserFromToken(token);

		return bestTeamService.getTeam(user);
	}

}
