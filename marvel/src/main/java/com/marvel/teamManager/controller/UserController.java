package com.marvel.teamManager.controller;

import java.nio.file.AccessDeniedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.marvel.teamManager.model.User;
import com.marvel.teamManager.model.UserCredentials;
import com.marvel.teamManager.repository.UserRepo;
import com.marvel.teamManager.service.UserService;

/**
 * Actions exposées pour gérer l'utilisateur
 * 
 * @author Caradoc
 *
 */
@CrossOrigin
@RestController
@RequestMapping(path = "/user")
public class UserController {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private UserService userService;

	@PostMapping(path = "/add")
	public @ResponseBody String addUser(@RequestBody UserCredentials bodyUser) {
		User user = new User(bodyUser);
		userRepo.save(user);
		return "user registred";
	}

	/**
	 * Tente de connecter l'utilisateur
	 * 
	 * @param bodyUser
	 * @return un token d'identification
	 * @throws AccessDeniedException utilisateur inconnu
	 */
	@PostMapping(path = "/signUp")
	public @ResponseBody String connectUser(@RequestBody UserCredentials bodyUser) throws AccessDeniedException {

		return userService.connectUser(bodyUser);
	}
}
